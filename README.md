# Welcome to the ethkeyscape repo
[![pipeline status](https://gitlab.com/petroff.ryan/ethkeyscape/badges/master/pipeline.svg)](https://gitlab.com/petroff.ryan/ethkeyscape/commits/master)
You might find a vizualition of the keyscape
and a proof-by-attack that the keyscape is super secure    
http://petroff.ryan.gitlab.io/ethkeyscape/

For fun bonus points, just keep adding technologies!
	- host the page on IPFS!
	- generate vanity addresses and QR codes!
	- generate 3D printable Vanity Vaults!
	- Order and pay (with crypto!) for printed/shipped Vanity Vaults


This is 'optimized enough' for today by using smaller text strings.
This is for future optimization: https://jsfiddle.net/h9sub275/68/
To use textures for the text instead of vertices and faces. Later.
