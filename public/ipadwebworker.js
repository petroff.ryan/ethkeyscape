//webworker.js
importScripts('keythereumPATCHED.js');

var keystore = []
function newKeychain () {
  var dk = keythereum.create();
  var ethAddress = keythereum.privateKeyToAddress(dk.privateKey);
  var ethPubkey = keythereum.privateKeyToPublic(dk.privateKey);
  return {priv:dk.privateKey.toString('hex'),pub:ethPubkey,addr:ethAddress};
}

function replenishKeystore(){
  console.log("repleneshing Keystore")
  for (var i = 0; i<50; i++){
    keystore.push(newKeychain());
  }
  console.log("Keystore replenished")
}

onmessage = function onmessage(e) {
//  console.log('Message received from main script', e.data);
  //if (e.data == "Requesting Keys, Please") {
    var thisKeychain = keystore.pop();
  //  console.log(thisKeychain);
    postMessage(thisKeychain); //JSON.stringify(?
    if(keystore.length<20){replenishKeystore();}
  //}
}

keystore.push(newKeychain())
keystore.push(newKeychain())
var firstKeychain = keystore.pop();
postMessage(firstKeychain);
replenishKeystore();
