//webworker.js
//var keythereum = new Worker("keythereumPATCHED.js");
importScripts('keythereumPATCHED.js');

function newKeychain () {
  var dk = keythereum.create();
  var ethAddress = keythereum.privateKeyToAddress(dk.privateKey);
  var ethPubkey = keythereum.privateKeyToPublic(dk.privateKey);
  return {priv:dk.privateKey.toString('hex'),pub:ethPubkey,addr:ethAddress};
}

//var keychain1 = {priv:"4f17ccf8a9cfdfdfd556e6e85f501549f9a4d91f582ae1787dc60fc2d6b82438",pub:"9cfdfdfd4f17ccf8a9cfdfdfd556e6e85f501549f9a4d91f582ae1787dc60fc2d6b8243855c675e4724fbd568f57e9bd1c5246f0b845fd51bc",addr:"0xc675e4724fbd568f57e9bd1c5246f0b845fd51bc"}
var keystore = []
keystore.push(newKeychain())

function replenishKeystore(){
  console.log("repleneshing Keystore")
  for (var i = 0; i<50; i++){
    keystore.push(newKeychain());
  }
  console.log("Keystore replenished")
}

replenishKeystore();

onmessage = function(e) {
//  console.log('Message received from main script', e.data);
  //if (e.data == "Requesting Keys, Please") {
    var thisKeychain = keystore.pop();
  //  console.log(thisKeychain);
    postMessage(thisKeychain); //JSON.stringify(?
    if(keystore.length<20){replenishKeystore();}
  //}
}

var firstKeychain = keystore.pop();
postMessage(firstKeychain);
