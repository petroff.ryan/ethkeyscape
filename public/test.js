// Welcome to EthKeyScape!
// Step 1: generate new random secret key
// Step 2: produce public address for that key
// Step 3: check that address for activity
// Step 4: Display results. Stop and make a big deal if non-zero.
// Step 5: Repeat (This currently handled outside this script using ```for n in {1..99}; do node test.js; done; cat win.txt``` in bash)



// Optional: Pretty up the console output
var yellowUnderline = '\033[4;33m';
var blueText = '\33[34m';
var redBG = '\33[41m';
var reset = '\033[0m';
//console.log("url: " + yellowUnderline + "http://superuser.com" + reset);


// Step one: Generate new random secret key
var keythereum = require("keythereum");
var dk = keythereum.create();

// Step two: Produce public address for that key
var keyObject = keythereum.dump("password", dk.privateKey, dk.salt, dk.iv);
var ethAddress = keyObject.address;

// Step three: Check for a balance
var api = require('etherscan-api').init('SNAKFY5725DX14Q926R9J79G2E6GNMYYUW');
var balance = api.account.balance(ethAddress);

// Step three and a half: check for transactions
var txlist = api.account.txlist('0x' + ethAddress.toString('hex'), 1, 'latest', 'asc');
txlist.
  then(function(txData){console.log('' + redBG + "Transactions found!", txData, '' + reset)}).
  catch(function(txData){/*console.log(txData)*/});

// Step four: Display results
balance.then(function(balanceData){
  if (balanceData.result == 0) {
        console.log("0 balance for key", '' + blueText + dk.privateKey.toString('hex') + reset, "@", '' + yellowUnderline +'https://etherscan.io/address/'+'0x' + ethAddress.toString('hex') + reset)
      }  else {
	console.log('' + redBG + "HOLY SHIT! FOUND " + reset , balanceData.result, '' + redBG + "WEI!" + reset)
  	console.log("New Key&Address ", dk.privateKey.toString('hex'), " & ", '' + yellowUnderline + 'https://etherscan.io/address/'+'0x' + ethAddress.toString('hex') + reset);
	saveWinners();
  }
});

// Step four and a half: Save winners to file
fs = require('fs');
var saveWinners = function () {
fs.appendFile('win.txt', '\n' + 'KEY: ' + dk.privateKey.toString('hex') + '   and ADDRESS: ' + 'https://etherscan.io/address/'+'0x'+ ethAddress.toString('hex') + '\n', function (err) {
    if (err) 
        return console.log(err);
    console.log('' + redBG + 'Wrote WINNING KEY AND ADDRESS TO FILE! :)' + reset);
});
};

// note: colour info came from https://misc.flogisoft.com/bash/tip_colors_and_formatting
