
// EthKeyScape.js by Ryan Petroff
//
// Generate random Ethereum keys
// Visualize keyspace connections
// Check addresses for transactions
//
// It's like a crazy lottery
// The chance of winning is almost 0
// But the potential reward is almost infinite
//
// ETHKeyScape demonstrates the security of Ethereum's key mechanisms
// and helps people reassure themselves their keys can be pretty safe.

// Generate
// Visualize
// Query live chain - make an incredible ruckus on hit. (Test this with an interceptor/packet replay)


// Step one: Generate new random secret key
var keythereum = require("keythereum");       // This means we'll need a bundler - webpack?
var dk = keythereum.create();

// Step two: Produce public address for that key
var keyObject = keythereum.dump("password", dk.privateKey, dk.salt, dk.iv);
var ethAddress = keyObject.address;
console.log("Key:",dk.privateKey.toString('hex'), "@", 'https://etherscan.io/address/'+'0x' + ethAddress.toString('hex'))

// Step three: Map it on the KeyScape
var ethkeyscape ;//...= imported function
// ethkeyscape.addKeys(dk.privateKey.toString('hex'),ethAddress.toString('hex'))

// Step four: Check the address for network transactions
var api = require('etherscan-api').init('SNAKFY5725DX14Q926R9J79G2E6GNMYYUW');  // obfuscate my key somehow?
var txlist = api.account.txlist('0x' + ethAddress.toString('hex'), 1, 'latest', 'asc');  //test this for sure
txlist.
  then(function(txData){saveWinners();console.log("Transactions found!", txData)}).
  catch(function(txData){console.log(txData)});

// This should never get called, but in case there's a hit, the world needs to know.
var saveWinners = function () {
  // How to notify?
  // Maybe the script should sign a transaction to transfer the ether to my address
  // and send me the key in a side-channel
}
